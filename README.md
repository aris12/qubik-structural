# Qubik Structural
This repository contains all files for the structural of Qubik 1p-PocketQube.
Parts are PCB's and mechanical parts.

## Development
For mechanical part is used FreeCAD 0.18. Also for FreeCAD files (.FCstd) use git LFS.

For PCB is used KiCAD 5.1 or later. If a footprint or symbol doesn't exist
in KiCAD libraries, then should become merge request in [lsf-kicad-lib](https://gitlab.com/librespacefoundation/lsf-kicad-lib)


## License
Licensed under the [CERN OHLv1.2](LICENSE)
[![Libre Space Foundation](https://img.shields.io/badge/%C2%A9%202014--2019-Libre%20Space%20Foundation-6672D8.svg)](https://librespacefoundation.org/)