EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L lsf-kicad:PQ9-Connector PQ1
U 1 1 5E3D54EC
P 6250 3225
F 0 "PQ1" H 6598 3193 50  0000 L CNN
F 1 "PQ9-Connector" H 6598 3102 50  0000 L CNN
F 2 "lsf-kicad-lib:PQ9" H 6650 2725 50  0001 C CNN
F 3 "https://libre.space/pq9ish" H 6750 3425 50  0001 C CNN
	1    6250 3225
	1    0    0    -1  
$EndComp
NoConn ~ 6250 3825
NoConn ~ 5850 3425
NoConn ~ 5850 3325
NoConn ~ 5850 3225
NoConn ~ 5850 3125
NoConn ~ 5850 3025
NoConn ~ 5850 2925
NoConn ~ 5850 2825
$EndSCHEMATC
